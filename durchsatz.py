#!/usr/bin/python3
#! -*- coding: utf-8 -*-

"""
v2:
take data from influxdb
Show current throughput of my Fritzbox 7390 on two analog Amp-meters

Ressources:
Raspberry Pi 2 B Pinout: http://www.element14.com/community/docs/ \
DOC-73950/l/raspberry-pi-2-model-b-gpio-40-pin-block-pinout
"""

import time
#from fritzconnection import FritzConnection
import requests
import RPi.GPIO as GPIO
import os
import sys
from datetime import datetime

PATH_TO_QUEUE = '/tmp/'



class Unifi:
    def __init__(self):
        pass
        self.url = 'http://10.0.2.71:8086/api/v2/query?org=danst'

        self.headers = {"Content-Type": "application/vnd.flux",
                        'Accept': 'application/csv', 
                        'Authorization': 'Token KzdbYYYNS_nR8RkKB3aOQ5Y7zGXLN1tdnclA-jfUKhH8MQ1393QsbJfg1ER9oaqipAY3AG606JdN8jiePd9auw=='}
 
    def gen_data(self, measurement='unifipoller_device_wan_receive_rate_bytes'):
        data = 'from(bucket: "unifi")|> range(start: -1d)|> filter(fn: (r) => r["_measurement"] == "'
        data += measurement
        data += '")|> group() |> keep(columns: ["_value"]) |> last()'
        
        #print(data)
        return data

    # unifipoller_device_wan_transmit_rate_bytes
    # unifipoller_device_wan_receive_rate_bytes

    


    """
    curl --request POST \
    http://10.0.2.71:8086/api/v2/query?orgID=INFLUX_ORG_ID  \
    --header 'Authorization: Token INFLUX_TOKEN' \
    --header 'Accept: application/csv' \
    --header 'Content-type: application/vnd.flux' \
    --data 'from(bucket:"example-bucket")
            |> range(start: -12h)
            |> filter(fn: (r) => r._measurement == "example-measurement")
            |> aggregateWindow(every: 1h, fn: mean)'
    """

    def get_last_value_influxdb(self, measurement):
        response = requests.post(self.url, data=self.gen_data(measurement), headers=self.headers)
        if response.status_code == 200:
            result = str(response.content).split(',')[-1]
            #print(result)
            result = int(result.replace('\\r','').replace('\\n','').replace("'",''))
        else:
            print('Error', "Status Code", response.status_code)
            result = None
        return result
        

    def get_mbps(self):
        """Return inflow, outflow as tuple"""
        res1 = self.get_last_value_influxdb('unifipoller_device_wan_receive_rate_bytes')
        res2 = self.get_last_value_influxdb('unifipoller_device_wan_transmit_rate_bytes')
        #print(res1/1000000, res2/1000000)
        return res1, res2
        
class CombineDrehspul:
    """Combine multiple Drehspul classes
    
    By combining the classes it is possible to change values syncronously
    """

    def __init__(self):
        self.drehspulen = []
    
    def add(self, drehspule):
        """ Füge Drehspulanzeige hinzu, initiiere letzen Wert mit Null
        """
        self.drehspulen.append(drehspule)

    def set_values(self, werte, calibration):
        """ Setze neue Werte für die Drehspulanzeigen
        
        nacheinander immer in 10er Schritten
        """
        
        # Adjust calibration, if new
        for num, drehspule in enumerate(self.drehspulen):
            drehspule.calibrate(calibration[num])
        
        max_calibration = 0
        for single in calibration:
            max_calibration = max(max_calibration, single['max'])
        single_step = int(max_calibration/30)
        delay = 0.05
        max_delta = 0
        new_werte = []
        for wert in werte:
            new_werte.append(int(wert+0.5))
        werte = new_werte
        
        
        no_changes = []
        for num, drehspule in enumerate(self.drehspulen):
            #print werte[num], last_wert, abs(werte[num]-last_wert)
            cur_delta = abs(werte[num]-drehspule.get_last_value())
            max_delta = max(max_delta, cur_delta)
            if cur_delta <= 0.01:
                no_changes.append(num)
                
        #import pdb; pdb.set_trace()
        # only proceed if we have changes in at least one value
        if len(no_changes) < 2:
            #print max_delta, single_step,  max_delta / single_step
            number_of_steps = max_delta / single_step
            for i in range(int(number_of_steps)):
                #print 'Step', i
                for num, drehspule in enumerate(self.drehspulen):
                    if not num in no_changes:
                        zwischenwert = drehspule.get_last_value() + \
                        i * (werte[num]-drehspule.get_last_value())/ \
                        float(number_of_steps)
                        drehspule.set_value(zwischenwert)

                time.sleep(delay)

        # Setze letzten Wert
        for num, drehspule in enumerate(self.drehspulen):
            if not num in no_changes:
                drehspule.set_value(werte[num])



class Drehspul:
    """One Drehspul monitor to display values between 0..100
    
    Drehspul monitor is attached to one GPIO pin at the Raspi
    """

    def __init__(self, port, calibration={'min': 0, 'max': 100}):
        self.port = port
        # create an object p for PWM on port 25 at 50 Hertz  
        GPIO.setup(self.port, GPIO.OUT) 
        self.device = GPIO.PWM(self.port, 50)
        self.calibration = calibration
        #print('cal init', calibration)
        self.device.start(0)
        self.last_value = 0
        self.max_calibration = self.calibration['max']

    def calibrate(self, calibration):
        """Change calibration values for current Drehspul class"""
        
        # Last value is not calibrated, therefore recalibrate now
        self.set_last_value(
            calibration['max'] / float(self.calibration['max'])
            * self.get_last_value())
        if 'offset' not in calibration.keys():
            calibration['offset'] = 0
        self.calibration = calibration

        #print('cal recall', calibration)
        self.max_calibration = self.calibration['max']
    
    def get_last_value(self):
        """Return last value set"""
        
        #print('get last val', self.last_value)
        return int(self.last_value+0.5)
    
    def set_last_value(self, last_value):
        """Set last_value to another value"""
        
        self.last_value = int(last_value+0.5)
        #print('set last value', self.last_value)
    
    def set_raw_value(self, value):
        """Set raw value at the GPIO (PWM).
        
        This value has to be between 0 and 100
        """
        # offset value
        offset = self.calibrate_value(self.calibration['offset'])
        # make sure value is between 0 and 100
        offset_value = max(min(value + offset, 100), 0)
        self.device.ChangeDutyCycle(offset_value) 
    
    def calibrate_value(self, value):
        """Recalculate value based on current calibration"""
        
        raw_value = (100.0/float(self.calibration['max']) * value)
        return raw_value
    
    def set_value(self, value):
        """Set value for Drehspule
        
        Value will be translated to raw_value
        (bound by calibration values)
        Value will also be stepped if required     
        """
        #raw_value = (100.0/float(self.calibration['max']) * value)
        #print('Value', value, 'Raw_value', raw_value)
        # Minimum not currently taken into account
        #import pdb; pdb.set_trace()
        try:
            percentage_change = (value - self.get_last_value())/ \
                float(self.get_last_value())
        except:
            percentage_change = 0.11
        if abs(percentage_change) > 0.10:
            if value > self.get_last_value():
                step = max(1, int(self.max_calibration/30))
            else:
                step = min(-1, -int(self.max_calibration/30))
            for i in range(int(self.get_last_value()), int(value), step):
                time.sleep(0.05)
                self.set_raw_value(self.calibrate_value(i))
        self.set_raw_value(self.calibrate_value(value))
        self.set_last_value(value)
        
    def cleanup(self):
        """Clean up after use
        
        Set value to 0 and stop device
        """
        self.set_value(0)
        self.device.stop()

class Button:
    """Represents BUTTON of Speedometer"""

    def __init__(self, port):
        self.port = port
        self.long_press = 4
        self.press_counter = 0
        self.last_pressed = False
        self.result = 0
        self.shutdown_requested = False
        self.key = 0
        GPIO.setup(self.port, GPIO.IN, pull_up_down=GPIO.PUD_UP)
        
    def get_pressed(self):
        """Check and return current BUTTON status"""
        
        return not GPIO.input(self.port)
    
    def check(self):
        """Check for BUTTON press
        
        Function has to be called regularly
        Different length of presses are differentiated
        Final results are returned as inverted values
        """
        result = 0
        if self.get_pressed():
            self.press_counter += 1
            self.last_pressed = True
            #print self.press_counter, self.long_press
            result = min(100, self.press_counter*100/ \
                float(self.long_press))
        else:
            if self.last_pressed:
                result = -1 * self.key
            else:
                result = 0
            self.press_counter = 0
            self.last_pressed = False
        self.key = result
        return result
    
    def use_key(self):
        """Define actions based on key press"""
        
        result = ''
        if self.key == -100:
            if self.shutdown_requested:
                # With second -100 we have to shutdown
                result = 'shutdown'
            else:
                # first -100 => shutdown request
                self.shutdown_requested = True
                print('Shutdown requested')
        else:
            self.shutdown_requested = False
            if self.key == -25:
                result = 'show_time'
        return result
            

class Fritzbox:
    """Connect to Fritzbox
    
    Wrapper for FritzboxConnection class
    """
    
    def __init__(self):
        try:
            self.fritz = FritzConnection()
        except IOError:
            # second try after 10 sec
            print('Second try after 10 sec.')
            time.sleep(10)
            self.fritz = FritzConnection()

    def get_mbps(self):
        """Get information on transfered data from Fritzbox"""
        
#print self.fritz.call_action('WANIPConnection', 'GetExternalIPAddress')
        try:
            info = self.fritz.call_action(
                    'WANCommonInterfaceConfig', 'GetAddonInfos')
            inflow = info['NewByteReceiveRate']
            outflow = info['NewByteSendRate']
        except:
            print('Exception', sys.exc_info()[0])
            inflow, outflow = 0, 0
        return inflow*8, outflow*8

def current_millis():
    """Return current time in milli seconds"""
    return int(round(time.time() * 1000))

def date_string():
    return '{:%Y-%m-%d %H.%M.%S}: '.format(datetime.now())

def date_print(arg):
    print(date_string() + arg)

if __name__ == '__main__':
    GPIO.setwarnings(False)
    GPIO.setmode(GPIO.BCM)  # choose BCM or BOARD numbering schemes.    
    FB = Unifi()
    LCD_INFLOW = Drehspul(15)
    LCD_OUTFLOW = Drehspul(18)
    BUTTON = Button (14)

    DREHSPULEN = CombineDrehspul()
    DREHSPULEN.add(LCD_INFLOW)
    DREHSPULEN.add(LCD_OUTFLOW)
    for i in range(0, 100):
        DREHSPULEN.set_values((i, i),
                              ({'min': 0, 'max': 100}, {'min': 0, 'max': 100}))
        time.sleep(0.05)
    for i in range(100, 0, -1):
        DREHSPULEN.set_values((i, i),
                              ({'min': 0, 'max': 100}, {'min': 0, 'max': 100}))
        time.sleep(0.05)

    #max_inflow, max_outflow = FB.get_mbps()
    LAST_MILLIS = current_millis()
    INFLOW, OUTFLOW = 0, 0 
    #normalized_inflow, normalized_outflow = 0, 0
    KEY = 0
    ACTION = ''
    try:
        while True:
            time.sleep(0.2)

            if current_millis() - LAST_MILLIS > 3000 and KEY == 0:
                #print('every five')
                LAST_MILLIS = current_millis()
                INFLOW, OUTFLOW = FB.get_mbps()
                
                # Output inflow and outflow to file
                # handle_pipe = open(PATH_TO_QUEUE + 'outflow.pipe', 'w')
#                 handle_pipe.write(OUTFLOW)
#                 handle_pipe.close()
#                 handle_pipe = open(PATH_TO_QUEUE + 'inflow.pipe', 'w')
#                 handle_pipe.write(INFLOW)
#                 handle_pipe.close()
                
                if (INFLOW > 5000000) or (OUTFLOW > 500000):
                    date_print(f'High in- or outflow with in: {INFLOW/1000000} Mbps, out: {OUTFLOW/1000000} Mbps')
                DREHSPULEN.set_values((INFLOW, OUTFLOW),
                    ({'min': 0, 'max': 120000000},
                    {'min': 0, 'max': 12000000})) # War 120 000 000 statt 60 000 000

            KEY = BUTTON.check()
            
            if KEY > 0:
                date_print('Registered key press {0}'.format(KEY))
                #import pdb; pdb.set_trace()
                DREHSPULEN.set_values((KEY, KEY),
                ({'min': 0, 'max': 100}, {'min': 0, 'max': 100}))
                #print('Continue')
            elif KEY < 0:
                date_print('Registered key release {0}'.format(KEY))
                ACTION = BUTTON.use_key()
                if ACTION == 'shutdown':
                    break
                elif ACTION == 'show_time':
                    HOUR = datetime.now().hour
                    MINUTE = datetime.now().minute
                    date_print('Current Hour {0}, Minute {1}'.format(HOUR, MINUTE))
                    DREHSPULEN.set_values((HOUR, MINUTE),
                        calibration=({'min': 0, 'max': 30, 'offset': 3},
                            {'min': 0, 'max': 75, 'offset': 7.5}))
                    time.sleep(5.0)
                    date_print('Continue normal operation')
            else:
                pass
    except (KeyboardInterrupt, SystemExit):
        pass
    date_print('Exiting gracefuly')
    LCD_INFLOW.cleanup()
    LCD_OUTFLOW.cleanup()
    GPIO.cleanup()  
    if ACTION == 'shutdown':
        os.system("shutdown now -h")
    
